if exists (
	select * from sys.views
	where name = 'PitstopNumbers' and schema_id = schema_id('test')
)
drop view test.PitstopNumbers
go

create view test.PitstopNumbers as (
	select distinct (
			select count(*)
			from pitstoptimes pt
			where r.stage_id = pt.stage_id and
				  r.pilot_id = pt.pilot_id
		) as pitstop_n
	from results r
)
go
