if exists (
	select * from sys.views
	where name = 'CountryLocals' and schema_id = schema_id('dbo')
)
drop view CountryLocals
go

create view dbo.CountryLocals as (
	select c.name as country, (
			select isnull(sum(t1.x), 0)
			from (
				select count(*) as x
				from city ci1 inner join stage s1 on ci1.city_id = s1.city_id
				   			  inner join results r on r.stage_id = s1.stage_id
							  inner join team t on r.team_id = t.team_id
				where t.country_id = c.country_id and
					  r.place_taken = 1 and
					  c.country_id = ci1.country_id
				group by s1.stage_id
			) t1
		) as local_wins, isnull(1.0*avg(s.audience), 0) as avg_audience
	from country c left outer join city ci on c.country_id = ci.country_id
				   left outer join stage s on ci.city_id = s.city_id 
	group by c.name, c.country_id
)
go

if exists (
	select * from sys.views
	where name = 'CountryLocalsNew' and schema_id = schema_id('dbo')
)
drop view CountryLocalsNew
go

create view dbo.CountryLocalsNew as (
	select CL.country as country, 
		CL.local_wins as local_wins,
		case CL.avg_audience
			when max(CL.avg_audience) 
				then convert(varchar, isnull(max(CL.avg_audience), 0)) + ' (max)'
			when min(CL.avg_audience) 
				then convert(varchar, isnull(min(CL.avg_audience), 0)) + ' (min)'
			else convert(varchar, isnull(avg(CL.avg_audience), 0))
		end as avg_audience
	from CountryLocals CL
	group by CL.country, CL.local_wins, CL.avg_audience
)
go

select * from CountryLocalsNew

/*
audience -> min, max

for each year -> months, races
*/

