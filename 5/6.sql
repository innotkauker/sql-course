if exists (
	select * from sys.views
	where name = 'CountryCities' and schema_id = schema_id('dbo')
)
drop view CountryCities
go

create view dbo.CountryCities as (
	select c0.[name] as country, c1.name as city
	from country c0 join city c1 on c1.country_id = c0.country_id
)
go

select * from CountryCities