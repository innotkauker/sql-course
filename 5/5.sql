if exists (
	select * from sys.views
	where name = 'YearMontsStages' and schema_id = schema_id('dbo')
)
drop view YearMontsStages
go

create view dbo.YearMontsStages as (
	-- select datename(month, x) as month
	-- from tyres
	-- where x between '2013-01-01' and '2013-12-01'
	-- select top 12
	-- 	datename(month, dateadd(month, row_number() over (order by object_id) - 1,0)) as [month]
	-- from sys.columns
	-- union
	select datepart(year, s.date) as [year], (
			select top 12
				datename(month, dateadd(month, row_number() over (order by object_id) - 1,0)) as month_name,
				dateadd(month, row_number() over (order by object_id) - 1,0) as m
			from sys.columns
		) as [month], count(*) as num
	from stage s
	where datepart(year, s.date) = datepart(year, s.date) and
		datepart(month, s.date) = m
	group by datepart(year, s.date), m
)
go

select * from YearMontsStages