use [Formula 1];
if exists (
		select name from sysobjects
		where name = 'StageLaps' 
			and type = 'TR'
		)
   DROP TRIGGER StageLaps
GO

create trigger StageLaps
	on Stage after update as
begin
	set nocount on;
	if update(lap_length)
	begin
		update Stage
		set total_laps = ROUND(300.0/i.lap_length, 0)
		from deleted s join inserted i on s.stage_id = i.stage_id
			where s.lap_length != i.lap_length
				and Stage.stage_id = i.stage_id
				and (s.date is null or s.date > GETDATE());
	end;
end;