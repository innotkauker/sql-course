-- serializable read and phantom read
set tran isolation level serializable;
begin tran;




update Results
	set place_taken = 3
	where stage_id = 6 
		and pilot_id = 10;	
commit;