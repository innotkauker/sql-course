set tran isolation level read uncommitted;
begin tran;
update Results
	set place_taken = case
		when place_taken < 20 
			then place_taken - 1
		else 20
		end
	where stage_id = 6;

commit;

select * from Results;