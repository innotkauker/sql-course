-- read uncommited & lost update
set tran isolation level read uncommitted;
begin tran;
update Results
	set place_taken = case
		when place_taken < 20 
			then place_taken + 1
		else 100
		end
	where stage_id = 6;
commit;


-- read uncommited and dirty read
set tran isolation level read uncommitted;
begin tran;
update Results
	set place_taken = 1
	where stage_id = 6 
		and pilot_id = 5;
commit;