-- repeatable read and unrepeatable read
set tran isolation level repeatable read;
begin tran;




update Results
	set place_taken = 5
	where stage_id = 6 
		and pilot_id = 5;
		
		
		
		
commit;