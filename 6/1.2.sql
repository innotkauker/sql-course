
set tran isolation level read uncommitted;
begin tran;
update Results
	set place_taken = 6
	where stage_id = 6 and pilot_id = 1;

select * from Results;
commit;