declare commands cursor for

select 'sp_addrolemember 'db_datareader', N'" + name + "''
	from [master].[sys].[database_principals]
	where type != 'R'
	
EXEC sp_addrolemember N'db_datareader', N'your-user-name'

declare @cmd varchar(max)

open commands
fetch next from commands into @cmd
while @@FETCH_STATUS=0
begin
  exec(@cmd)
  fetch next from commands into @cmd
end

close commands
deallocate commands

-- select 'grant select on ' + name + ' to public'
-- from sys.tables

alter login test
	with default_database = [Formula 1];

  sp_msforeach @command1= "?..sp_addrolemember N 'db_datareader', N 'username'"
 
 EXEC sp_MSForeachdb "?..sp_addrolemember N 'db_datareader', N 'public'"
 
-- exec sp_addrolemember 'db_datareader', 'public'
