grant select, insert, update
	on dbo.results
	to test;
	
grant select (stage_id, [date], city_id, title, lap_length, total_laps), 
	  update ([date], title, total_laps)
	on dbo.stage
	to test;
	
grant select
	on dbo.city
	to test;
	
grant select
	on dbo.country
	to test;
	
grant select
	on dbo.countrylocals
	to test;
	
-- drop role cs_upd;
create role cs_upd;

grant select (city)
	on dbo.countrycities
	to cs_upd;
	
grant update (country)
	on dbo.countrycities
	to cs_upd;
	
-- grant cs_upd
--	to test;
	
exec sp_addrolemember 'cs_upd', 'test'
-- exec sp_droprolemember 'db_owner', 'test'

exec sp_addrolemember '', 'test'

------------------

set tran isolation level read uncommitted
select * from CountryCities