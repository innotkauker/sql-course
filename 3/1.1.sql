select results.pilot_id, count(stage_id) as stages, sum(score) as total
from results inner join pilot on pilot.pilot_id = results.pilot_id
			 inner join scores on results.place_taken = scores.place_taken
group by results.pilot_id
order by total desc