with TyreScores as (
	select results.stage_id, manufacturer.title, scores.score
	from results join resulttyres on results.stage_id = resulttyres.stage_id and
										   results.pilot_id = resulttyres.pilot_id
				 join tyres on ResultTyres.tyres_id = tyres.tyres_id
				 join manufacturer on tyres.manufacturer_id = manufacturer.manufacturer_id
				 join scores on results.place_taken = scores.place_taken
	where ResultTyres.start_time = convert(time, '00:00:00', 108)
) 

select stage_id
from Stage
group by stage_id
having (
	select SUM(TyreScores.score)
	from TyreScores
	where TyreScores.title = 'Michelin' and TyreScores.stage_id = Stage.stage_id
) > (
	select SUM(TyreScores.score)
	from TyreScores
	where TyreScores.title != 'Michelin' and TyreScores.stage_id = Stage.stage_id
)