/*select car with the most powerful engine for each stage where the car carrying it
 has finished over 50% distance*/

select s.stage_id, MAX(e.power)
from engine e inner join car c on c.engine_id = e.engine_id
			  inner join results r on r.car_id = c.car_id
			  inner join stage s on s.stage_id = r.stage_id
			  inner join (
			  		select r.stage_id, r.pilot_id, count(lap_n) as done
			  		from laps l inner join results r on l.stage_id = r.stage_id and 
			  										  l.pilot_id = r.pilot_id
			  		group by r.stage_id, r.pilot_id
			  		) t1 on t1.stage_id = r.stage_id and t1.pilot_id = r.pilot_id
where s.total_laps < 2*done
group by s.stage_id