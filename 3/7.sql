/*find such countries that there were no stages ran on their 
territory or they were ran without pilots from that country*/

with X as (
	select c.country_id, count(s.stage_id) as num
	from country c inner join city ci on ci.country_id = c.country_id
				   left outer join stage s on s.city_id = ci.city_id
				   left outer join results r on r.stage_id = s.stage_id
				   left outer join pilot p on r.pilot_id = p.pilot_id 
				   			and p.country_id = c.country_id
	group by c.country_id
)
select x.country_id
from x
where num = 0