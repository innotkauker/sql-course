/****** Object:  Table [TyresTypes]    Script Date: 10/13/2013 00:05:03 ******/
CREATE TABLE [TyresTypes](
	[type_id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NOT NULL,
	[color] [varchar](10),
CONSTRAINT [PK_TyresTypes] PRIMARY KEY 
(
	[type_id] ASC
)
);

CREATE TABLE [Country](
	[country_id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
CONSTRAINT [PK_Country_1] PRIMARY KEY 
(
	[country_id] ASC
)
);

CREATE TABLE [Pilot](
	[pilot_id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[country_id] [smallint] NOT NULL,
CONSTRAINT [PK_Pilot] PRIMARY KEY 
(
	[pilot_id] ASC
)
);

CREATE TABLE [Manufacturer](
	[manufacturer_id] [smallint] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[country_id] [smallint] NOT NULL,
CONSTRAINT [PK_EngineManufacturer] PRIMARY KEY 
(
	[manufacturer_id] ASC
)
);

CREATE TABLE [City](
	[city_id] [smallint] IDENTITY(1,1) NOT NULL,
	[country_id] [smallint] NOT NULL,
	[name] [varchar](30) NOT NULL,
CONSTRAINT [PK_City] PRIMARY KEY 
(
	[city_id] ASC
)
);

CREATE TABLE [Team](
	[team_id] [smallint] IDENTITY(1,1) NOT NULL,
	[country_id] [smallint] NOT NULL,
	[title] [varchar](50) NOT NULL,
CONSTRAINT [PK_Car] PRIMARY KEY 
(
	[team_id] ASC
)
);

CREATE TABLE [PilotTeam](
	[pilot_id] [smallint] NOT NULL, --int
	[team_id] [smallint] NOT NULL,
	[contract_start] [datetime] NOT NULL default getdate(), -- -month
	[contract_end] [datetime],
CONSTRAINT [PK_PilotTeam] PRIMARY KEY 
(
	[pilot_id] ASC,
	[team_id] ASC,
	[contract_start] ASC
)
);

CREATE TABLE [Engine](
	[engine_id] [smallint] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50),
	[manufacturer_id] [smallint] NOT NULL,
	[cylinders] [smallint],
	[volume] [smallint],
	[power] [smallint],
CONSTRAINT [PK_Engine] PRIMARY KEY 
(
	[engine_id] ASC
)
);

CREATE TABLE [Tyres](
	[tyres_id] [smallint] IDENTITY(1,1) NOT NULL,
	[manufacturer_id] [smallint],
	[type_id] [smallint] NOT NULL,
	[title] [varchar](50),
CONSTRAINT [PK_Tyres] PRIMARY KEY 
(
	[tyres_id] ASC
)
);

CREATE TABLE [Scores](
	[place_taken] [smallint] IDENTITY(1,1) NOT NULL,
	[score] [smallint] NOT NULL,
CONSTRAINT [PK_Scores] PRIMARY KEY 
(
	[place_taken] ASC
)
);

CREATE TABLE [Stage](
	[stage_id] [smallint] IDENTITY(1,1) NOT NULL,
	[date] [date],
	[audience] [int],
	[city_id] [smallint] NOT NULL,
	[title] [varchar](100) NOT NULL,
	[lap_length] [real] NOT NULL,
	[total_laps] [smallint],
CONSTRAINT [PK_Stage_1] PRIMARY KEY 
(
	[stage_id] ASC
)
);

CREATE TABLE [Car](
	[car_id] [smallint] IDENTITY(1,1) NOT NULL,
	[team_id] [smallint],
	[engine_id] [smallint],
CONSTRAINT [PK_Car_1] PRIMARY KEY 
(
	[car_id] ASC
)
);

CREATE TABLE [Results](
	[stage_id] [smallint] NOT NULL,
	[pilot_id] [smallint] NOT NULL,
	[team_id] [smallint] NOT NULL,
	[total_time] [time](7),
	[place_taken] [smallint],
	[car_id] [smallint] NOT NULL,
CONSTRAINT [PK_Results_1] PRIMARY KEY 
(
	[pilot_id] ASC,
	[stage_id] ASC
)
);

CREATE TABLE [ResultTyres](
	[stage_id] [smallint] NOT NULL,
	[pilot_id] [smallint] NOT NULL,
	[start_time] [time](7) NOT NULL,
	[tyres_id] [smallint] NOT NULL,
CONSTRAINT [PK_ResultTyres] PRIMARY KEY 
(
	[stage_id] ASC,
	[pilot_id] ASC,
	[start_time] ASC
)
);

CREATE TABLE [PitstopTimes](
	[stage_id] [smallint] NOT NULL,
	[pilot_id] [smallint] NOT NULL,
	[pitstop_n] [smallint] NOT NULL,
	[start_time] [time](7) NOT NULL,
	[pitstop_time] [time](7),
CONSTRAINT [PK_PitstopTimes] PRIMARY KEY 
(
	[stage_id] ASC,
	[pilot_id] ASC,
	[pitstop_n] ASC
)
);

CREATE TABLE [Laps](
	[stage_id] [smallint] NOT NULL,
	[pilot_id] [smallint] NOT NULL,
	[lap_n] [smallint] NOT NULL,
	[lap_time] [time](7),
CONSTRAINT [PK_Laps] PRIMARY KEY 
(
	[stage_id] ASC,
	[pilot_id] ASC,
	[lap_n] ASC
)
);

ALTER TABLE [Pilot]  
	ADD CONSTRAINT [FK_Pilot_Country] FOREIGN KEY([country_id])
		REFERENCES [Country] ([country_id]);

ALTER TABLE [Manufacturer]  
	ADD CONSTRAINT [FK_EngineManufacturer_Country] FOREIGN KEY([country_id])
		REFERENCES [Country] ([country_id]);

ALTER TABLE [City]  
	ADD CONSTRAINT [FK_City_Country] FOREIGN KEY([country_id])
		REFERENCES [Country] ([country_id]);

ALTER TABLE [Team]  
	ADD CONSTRAINT [FK_Team_Country] FOREIGN KEY([country_id])
		REFERENCES [Country] ([country_id]);

ALTER TABLE [PilotTeam]  
	ADD CONSTRAINT [FK_PilotTeam_Pilot] FOREIGN KEY([pilot_id])
		REFERENCES [Pilot] ([pilot_id]);

ALTER TABLE [PilotTeam]  
	ADD CONSTRAINT [FK_PilotTeam_Team] FOREIGN KEY([team_id])
		REFERENCES [Team] ([team_id]);

ALTER TABLE [PilotTeam]
	ADD CONSTRAINT [CK_PilotTeam_ConStart]
		CHECK (YEAR(getdate()) - YEAR([contract_start]) < 100);

ALTER TABLE [PilotTeam]
	ADD CONSTRAINT [CK_PilotTeam_ConEnd]
		CHECK (DATEDIFF(DAY, contract_start, contract_end) > 0);

ALTER TABLE [Engine]  
	ADD CONSTRAINT [FK_Engine_EngineManufacturer] FOREIGN KEY([manufacturer_id])
		REFERENCES [Manufacturer] ([manufacturer_id]);

ALTER TABLE [Engine]
	ADD CONSTRAINT [CK_Engine_cylinders] 
		CHECK (([cylinders] > 0) AND ([cylinders] <= 16));

ALTER TABLE [Engine]
	ADD CONSTRAINT [CK_Engine_volume] 
		CHECK (([volume] > 0.5) AND ([volume] <= 6));

ALTER TABLE [Engine]
	ADD CONSTRAINT [CK_Engine_power] 
		CHECK (([power] > 100) AND ([power] <= 1500));

ALTER TABLE [Tyres]  
	ADD CONSTRAINT [FK_Tyres_Manufacturer] FOREIGN KEY([manufacturer_id])
		REFERENCES [Manufacturer] ([manufacturer_id]);

ALTER TABLE [Tyres]  
	ADD CONSTRAINT [FK_Tyres_TyresTypes] FOREIGN KEY([type_id])
		REFERENCES [TyresTypes] ([type_id]);

ALTER TABLE [Stage]  
	ADD CONSTRAINT [FK_Stage_City] FOREIGN KEY([city_id])
		REFERENCES [City] ([city_id]);

ALTER TABLE [Stage]  
	ADD CONSTRAINT [CK_Stage_LapN]
		CHECK ([total_laps] > 0);

ALTER TABLE [Stage]  
	ADD CONSTRAINT [CK_Stage_LapLen]
		CHECK ([lap_length] > 0);

ALTER TABLE [Stage]  
	ADD CONSTRAINT [CK_Stage_Audience]
		CHECK ([audience] > 0);

ALTER TABLE [Car]  
	ADD CONSTRAINT [FK_Car_Engine1] FOREIGN KEY([engine_id])
		REFERENCES [Engine] ([engine_id]);

ALTER TABLE [Car]  
	ADD CONSTRAINT [FK_Car_Team] FOREIGN KEY([team_id])
		REFERENCES [Team] ([team_id]);

ALTER TABLE [Results]  
	ADD CONSTRAINT [FK_Results_Car1] FOREIGN KEY([car_id])
		REFERENCES [Car] ([car_id]);

ALTER TABLE [Results]  
	ADD CONSTRAINT [FK_Results_Pilot] FOREIGN KEY([pilot_id])
		REFERENCES [Pilot] ([pilot_id]);
         
ALTER TABLE [Results]  
	ADD CONSTRAINT [FK_Results_Stage] FOREIGN KEY([stage_id])
		REFERENCES [Stage] ([stage_id]);

ALTER TABLE [Results]  
	ADD CONSTRAINT [FK_Results_Team] FOREIGN KEY([team_id])
		REFERENCES [Team] ([team_id]);
		
ALTER TABLE [Results]  
	ADD CONSTRAINT [FK_Results_Scores] FOREIGN KEY([place_taken])
		REFERENCES [Scores] ([place_taken]);
       
ALTER TABLE [ResultTyres]  
	ADD CONSTRAINT [FK_ResultTyres_Results] FOREIGN KEY([pilot_id], [stage_id])
		REFERENCES [Results] ([pilot_id], [stage_id]);

ALTER TABLE [ResultTyres]  
	ADD CONSTRAINT [FK_ResultTyres_Tyres] FOREIGN KEY([tyres_id])
		REFERENCES [Tyres] ([tyres_id]);

ALTER TABLE [PitstopTimes]  
	ADD CONSTRAINT [FK_PitstopTimes_Results] FOREIGN KEY([pilot_id], [stage_id])
		REFERENCES [Results] ([pilot_id], [stage_id]);

ALTER TABLE [Laps]  
	ADD CONSTRAINT [FK_Laps_Results] FOREIGN KEY([pilot_id], [stage_id])
		REFERENCES [Results] ([pilot_id], [stage_id]);

ALTER TABLE [Laps]  
	ADD CONSTRAINT [CK_Laps_LapN]
		CHECK ([lap_n] > 0);

ALTER TABLE [Results]  
	ADD CONSTRAINT [CK_Results_place]
		CHECK (([place_taken] = NULL) OR ([place_taken] > 0) AND ([place_taken] < 24));

ALTER TABLE [TyresTypes]
	ADD CONSTRAINT [CK_TyresTypes_name_color] 
		CHECK (([name] = 'hard' AND [color] = 'orange') OR
		 	([name] = 'medium' AND [color] = 'white') OR 
		 	([name] = 'soft' AND [color] = 'yellow') OR
			([name] = 'super-soft' AND [color] = 'red') OR 
			([name] = 'intermediate' AND [color] = 'green') OR 
			([name] = 'full wet' AND [color] = 'blue'));
