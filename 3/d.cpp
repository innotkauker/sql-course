#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std;

static const char alphanum[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

int stringLength = sizeof(alphanum) - 1;
const int pilots = 10, stages = 10, cities = 20, countries = 20, teams = 10, manufacturers = 20,
		engines = 15, cars = 10, tyrestypes = 6, tyres = 10;


char genRandom()  // Random string generator function.
{
    return alphanum[rand() % stringLength];
}


int main(){
	srand((unsigned int)(time(0)));
	
	// cout << "SET IDENTITY_INSERT INTO [Pilot] ON" << endl;
	for(int i = 0; i < pilots; i++){
		std::cout << "INSERT INTO [Pilot] " <<
 			"VALUES (" << i + 1 << ", '";
		int t = rand() % 15 + 3;
		for(int z = 0; z < t; z++){
			cout << genRandom();
		}
		cout << "', " << rand() % countries + 1 << ")" << std::endl;
	}
	// cout << "SET IDENTITY_INSERT INTO [Pilot] OFF" << endl << endl;
	
	
	// cout << "SET IDENTITY_INSERT INTO [Team] ON" << endl;
	for(int i = 0; i < teams; i++){
		std::cout << "INSERT INTO [Team] " << ", " << rand() % countries + 1 <<
 			"VALUES (" << i + 1 << ", '";
		int t = rand() % 10 + 5;
		for(int z = 0; z < t; z++){
			cout << genRandom();
		}
		cout << "')" << std::endl;
	}
	// cout << "SET IDENTITY_INSERT INTO [Team] OFF" << endl << endl;
	
	
	// cout << "SET IDENTITY_INSERT INTO [PilotTeam] ON" << endl;
	for(int j = 0; j < pilots; j++){ // pilots
		int t = rand() % 4 + 1;
		for(int i = 0; i < t; i++){
			std::cout << "INSERT INTO [PilotTeam] " <<
 				"VALUES (" << j + 1 << ", " << rand() % pilots + 1;
			cout << ", convert(date, '" << rand() % 62 + 1950 << '-' << rand() % 12 + 1 << '-' << rand() % 28 + 1 << "', 102), ";
			cout << "convert(date, '" << 2013 << '-' << rand() % 12 + 1 << '-' << rand() % 28 + 1 << "', 102)";
			cout << ")" << std::endl;
		}
	}
	// cout << "SET IDENTITY_INSERT INTO [PilotTeam] OFF" << endl << endl;
	
	
	// cout << "SET IDENTITY_INSERT INTO [Manufacturer] ON" << endl;
	for(int i = 0; i < manufacturers; i++){
		std::cout << "INSERT INTO [Manufacturer] " <<
 			"VALUES (" << i + 1 << ", '";
		int t = rand() % 10 + 5;
		for(int z = 0; z < t; z++){
			cout << genRandom();
		}
		cout << "', " << rand() % countries + 1 << ")" << std::endl;
	}
	// cout << "SET IDENTITY_INSERT INTO [Manufacturer] OFF" << endl << endl;
	
	
	// cout << "SET IDENTITY_INSERT INTO [Engine] ON" << endl;
	for(int i = 0; i < engines; i++){
		std::cout << "INSERT INTO [Engine] " <<
 			"VALUES (" << i + 1 << ", '";
		int t = rand() % 10 + 5;
		for(int z = 0; z < t; z++){
			cout << genRandom();
		}
		cout << "', " << rand() % manufacturers + 1 << ", " << rand() % 12 + 1 << ", " << (rand() % 40) / 10.0 + 0.1 << ", " << (rand() % 1000) + 500  << ")" << std::endl;
	}
	// cout << "SET IDENTITY_INSERT INTO [Engine] OFF" << endl << endl;
	
	// cout << "SET IDENTITY_INSERT INTO [Car] ON" << endl;
	for(int i = 0; i < cars; i++){
		std::cout << "INSERT INTO [Car] " <<
 			"VALUES (" << i + 1 << ", " << rand() % teams + 1 << ", ";
		cout << rand() % engines + 1 << ")" << std::endl;
	}
	// cout << "SET IDENTITY_INSERT INTO [Car] OFF" << endl << endl;

	// cout << "SET IDENTITY_INSERT INTO [Results] ON" << endl;
	for(int i = 0; i < stages; i++){ // Stages
		// int t = rand() %pilots + 1;
		for(int j = 0; j < pilots; j++){ // pilots
			std::cout << "INSERT INTO [Results] " <<
 				"VALUES (" << i + 1 << ", " << j + 1 << ", " << rand() % teams + 1;
 			cout << ", " << "convert(time, '0" << rand() % 2 << ':' << rand() % 60 << ':' << rand() % 60 << "', 108)";
			cout << ", " << rand() % 20 + 1 << ", " << rand() % cars + 1;
			cout << ")" << std::endl;
		}
	}
	// cout << "SET IDENTITY_INSERT INTO [Results] OFF" << endl << endl;

	// cout << "SET IDENTITY_INSERT INTO [Laps] ON" << endl;
	for(int i = 0; i < stages; i++) // Stages
		for(int j = 0; j < pilots; j++){ // pilots
			int t = rand() % 100 + 1;
			for(int k = 0; k < t; k++){ // laps
			std::cout << "INSERT INTO [Laps] " <<
 				"VALUES (" << i + 1 << ", " << j + 1 << ", " << k + 1;
 			cout << ", " << "convert(time, '00:0"<< rand() % 9 << ':' << rand() % 60 << "', 108)";
			cout << ")" << std::endl;
		}
	}
	// cout << "SET IDENTITY_INSERT INTO [Laps] OFF" << endl << endl;

	// cout << "SET IDENTITY_INSERT INTO [PitstopTimes] ON" << endl;
	for(int i = 0; i < stages; i++) // Stages
		for(int j = 0; j < pilots; j++){ // pilots
			int t = rand() % 7 + 1; 
			for(int k = 0; k < t; k++){ // pitstops
			std::cout << "INSERT INTO [PitstopTimes] " <<
 				"VALUES (" << i + 1 << ", " << j + 1 << ", " << k + 1;
 			cout << ", " << "convert(time, '0" << rand() % 2 << ':' << rand() % 60 << ':' << rand() % 60 << "', 108)";
 			cout << ", " << "convert(time, '00:00:0" << rand() % 10 << "', 108)";
			cout << ")" << std::endl;
		}
	}
	// cout << "SET IDENTITY_INSERT INTO [PitstopTimes] OFF" << endl << endl;

	// cout << "SET IDENTITY_INSERT INTO [TyresTypes] ON" << endl;
	for(int i = 0; i < tyrestypes; i++){
		cout << "INSERT INTO [TyresTypes] " <<
 			"VALUES (" << i + 1 << ", 'TYRENAME', 'TYRECOL')";
		cout << endl;
	}
	// cout << "SET IDENTITY_INSERT INTO [TyresTypes] OFF" << endl << endl;

	// cout << "SET IDENTITY_INSERT INTO [Tyres] ON" << endl;
	for(int i = 0; i < tyres; i++){
		cout << "INSERT INTO [Tyres] " <<
 			"VALUES (" << i + 1 << ", " << rand() % manufacturers + 1 << ", " << rand() % tyrestypes << ", '" ;
 		int t = rand() % 10 + 5;
		for(int z = 0; z < t; z++){
			cout << genRandom();
		}
		cout << "')" << endl;
	}

	for(int i = 0; i < stages; i++) // Stages
		for(int j = 0; j < pilots; j++){ // pilots
			int t = rand() % 6 + 1;
			for(int k = 0; k < t; k++){ // switches
				cout << "INSERT INTO [ResultTyres] " <<
		 			"VALUES (" << i + 1 << ", " << j + 1 << ", ";
		 		cout << "convert(time, '0" << rand() % 2 << ':' << rand() % 60 << ':' << rand() % 60 << "', 108), ";
				cout << rand() % tyres + 1 << ")" << endl;
			}
	}
	// cout << "SET IDENTITY_INSERT INTO [ResultTyres] OFF" << endl << endl;
	return 0;
}

