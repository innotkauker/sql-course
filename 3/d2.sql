-- remove results for (3:1)

Begin transaction d2;
delete from Laps
where pilot_id = 1 and stage_id = 3;
delete from PitstopTimes
where pilot_id = 1 and stage_id = 3;
delete from resulttyres
where pilot_id = 1 and stage_id = 3;
delete from results
where pilot_id = 1 and stage_id = 3;

rollback transaction d2 