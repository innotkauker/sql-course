SELECT SUM(item.total - price.min_price * item.quantity) as result
FROM ITEM, PRODUCT, PRICE, SALES_ORDER
WHERE item.product_id = product.product_id AND
	product.description = 'SP TENNIS RACKET' AND
	price.product_id = product.product_id AND
	item.actual_price > price.min_price AND
	sales_order.order_id = item.order_id AND
	sales_order.order_date >= price.start_date AND
	sales_order.order_date <= CASE 
		WHEN price.end_date IS NOT NULL THEN price.end_date
		ELSE getdate()
		END