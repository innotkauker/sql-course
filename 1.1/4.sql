SELECT employee_id, [function]
FROM EMPLOYEE, DEPARTMENT, LOCATION, JOB
WHERE employee.department_id = department.department_id AND
	employee.job_id = job.job_id AND 
	department.location_id = location.location_id AND
	location.regional_group = 'New York' AND
	employee.salary IS NOT NULL AND
	salary/CONVERT(numeric, (getdate() - hire_date)) = (
		SELECT MAX(salary/CONVERT(numeric, (getdate() - hire_date)))
		FROM EMPLOYEE, DEPARTMENT, LOCATION
		WHERE employee.department_id = department.department_id AND
			department.location_id = location.location_id AND
			location.regional_group = 'New York' AND
			employee.salary IS NOT NULL)