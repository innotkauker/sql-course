SELECT city, regional_group, num
FROM (
	SELECT city, regional_group, COUNT(CUSTOMER.customer_id) as num
		FROM CUSTOMER, EMPLOYEE, DEPARTMENT, LOCATION, SALES_ORDER
		WHERE CUSTOMER.salesperson_id = EMPLOYEE.employee_id AND
				EMPLOYEE.department_id = DEPARTMENT.department_id AND
				LOCATION.location_id = DEPARTMENT.location_id AND 
				SALES_ORDER.customer_id = CUSTOMER.customer_id
		GROUP BY city, regional_group
	) x
WHERE num > 0
ORDER BY city 