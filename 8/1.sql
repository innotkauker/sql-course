--1)
exec sp_tables
	@table_owner = test;
	
select so.name, su.name
	from sysobjects so join sysusers su on so.uid = su.uid  
	where su.name = 'test';

-- this
select t1.name as [table], su.name as [user]
from (select o.name as name, 
				case
					when o.principal_id IS NULL then s.principal_id
					else o.principal_id
				end as principal_id
	from sys.objects o join sys.schemas s on o.schema_id = s.schema_id
	where o.type = 'U'
	) t1 join sys.sysusers su on su.uid = t1.principal_id
where su.name = 'test'
		
--2)---------------------------------------------
declare @tmp table (
	TABLE_QUALIFIER sysname, 
	TABLE_OWNER sysname,
	TABLE_NAME sysname,
	TABLE_TYPE varchar(32),
	REMARKS varchar(254) 
)
INSERT INTO @tmp (TABLE_QUALIFIER, TABLE_OWNER, TABLE_NAME, TABLE_TYPE, REMARKS) 
	exec sp_tables
		@table_owner = test
		
select * from @tmp

-- this <---
select t2.t_name as [table], c.name as [column], c.is_nullable as nullable, ty.name as [type], ty.max_length as [length]
from (select t1.ID as [table], t1.name as t_name, su.name as [user]
	from (select o.[object_id] as ID, o.name as name, 
				case
					when o.principal_id IS NULL then s.principal_id
					else o.principal_id
				end as principal_id
			from sys.objects o join sys.schemas s on o.schema_id = s.schema_id
			where o.type = 'U'
		 ) t1 join sys.sysusers su on su.uid = t1.principal_id 
	where su.name = 'test') t2 join sys.columns c on t2.[table] = c.object_id
							   join sys.types ty on ty.user_type_id = c.system_type_id
							   
--3)-----------------------------------------
select t1.name as [name], o.name as [table], t1.type as type, su.name as [user]
from (select o.name as name, o.parent_object_id as parent, o.type as [type], 
				case
					when o.principal_id IS NULL then s.principal_id
					else o.principal_id
				end as principal_id
	from sys.objects o join sys.schemas s on o.schema_id = s.schema_id
	where o.type = 'PK' or o.type = 'F'
	) t1 join sys.sysusers su on su.uid = t1.principal_id
		 join sys.objects o on o.object_id = t1.parent
where su.name = 'test'

--4)------------------------------------------
select t1.name as [name], o.name as [table], o1.name as foreign_table, su.name as [user]
from (select o.name as name, o.object_id as ID, o.parent_object_id as parent, o.type as [type], 
				case
					when o.principal_id IS NULL then s.principal_id
					else o.principal_id
				end as principal_id
	from sys.objects o join sys.schemas s on o.schema_id = s.schema_id
	where o.type = 'F'
	) t1 join sys.sysusers su on su.uid = t1.principal_id
		 join sys.objects o on o.object_id = t1.parent
		 join sys.foreign_keys fk on fk.object_id = t1.ID
		 join sys.objects o1 on fk.referenced_object_id = o1.object_id
where su.name = 'test'

--5)------------------------------------------
select t1.name as [view], t1.code as query, su.name as [user]
from (select v.name as name, m.definition as code,
				case
					when v.principal_id IS NULL then s.principal_id
					else v.principal_id
				end as principal_id
	from sys.views v join sys.schemas s on v.schema_id = s.schema_id
					 join sys.sql_modules m on v.object_id = m.object_id
	) t1 join sys.sysusers su on su.uid = t1.principal_id
where su.name = 'test'

--6)------------------------------------------
select t1.name as [trigger], t1.parent as [parent]
from (select o.name as name, o1.name as parent,
				case
					when o.principal_id IS NULL then s.principal_id
					else o.principal_id
				end as principal_id
	from sys.objects o join sys.schemas s on o.schema_id = s.schema_id
					   join sys.objects o1 on o.parent_object_id = o1.object_id
	where o.type = 'TA'
	) t1 join sys.sysusers su on su.uid = t1.principal_id
where su.name = 'test'