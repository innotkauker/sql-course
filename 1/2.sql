SELECT city, regional_group, customer_id,  (
	SELECT COUNT(*)
		FROM SALES_ORDER
		WHERE SALES_ORDER.customer_id = CUSTOMER.customer_id
	) AS number
	FROM CUSTOMER, EMPLOYEE, DEPARTMENT, LOCATION
	WHERE CUSTOMER.salesperson_id = EMPLOYEE.employee_id AND
			EMPLOYEE.department_id = DEPARTMENT.department_id AND
			LOCATION.location_id = DEPARTMENT.location_id AND 
			(SELECT COUNT(*)
				FROM SALES_ORDER
				WHERE SALES_ORDER.customer_id = CUSTOMER.customer_id
			) > 0
ORDER BY city 