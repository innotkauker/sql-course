SELECT first_name, last_name, [function]
FROM EMPLOYEE, JOB, DEPARTMENT, LOCATION
WHERE employee.job_id = job.job_id AND 
	employee.department_id = department.department_id AND
	department.location_id = location.location_id AND
	location.regional_group = 'New York'