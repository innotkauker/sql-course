use [Formula 1]
select * into ResultTyresBig from ResultTyres
select * into TyresBig from Tyres
select * into TyresBigIndex from Tyres
select * into ManufacturerBig from Manufacturer
select * into ManufacturerBigIndex from Manufacturer

go

---------------------

INSERT INTO [ResultTyresBig] 
	select stage_id, pilot_id, start_time, tyres_id
	from ResultTyres

go 3000

SET IDENTITY_INSERT TyresBig ON
go
INSERT INTO [TyresBig] (tyres_id, manufacturer_id, type_id, title)
	select tyres_id, manufacturer_id, type_id, title
	from Tyres
go 20000

INSERT INTO [TyresBig] (tyres_id, manufacturer_id, type_id, title)
	values (6, 7, 1, 'example')
SET IDENTITY_INSERT TyresBig OFF
go

SET IDENTITY_INSERT TyresBigIndex ON
go
INSERT INTO [TyresBigIndex] (tyres_id, manufacturer_id, type_id, title)
	select tyres_id, manufacturer_id, type_id, title
	from Tyres;
	
go 20000

INSERT INTO [TyresBigIndex] (tyres_id, manufacturer_id, type_id, title)
	values (6, 7, 1, 'example')
SET IDENTITY_INSERT TyresBigIndex OFF
go
---------------------
SET IDENTITY_INSERT ManufacturerBig ON
go
INSERT INTO [ManufacturerBig] (manufacturer_id, title, country_id)
	select manufacturer_id, title, country_id
	from Manufacturer
go 20000

INSERT INTO [ManufacturerBig] (manufacturer_id, title, country_id)
	values (7, 'm_example', 3)
SET IDENTITY_INSERT ManufacturerBig OFF
go

SET IDENTITY_INSERT ManufacturerBigIndex ON
go
INSERT INTO [ManufacturerBigIndex] (manufacturer_id, title, country_id)
	select manufacturer_id, title, country_id
	from Manufacturer;
go 20000

INSERT INTO [ManufacturerBigIndex] (manufacturer_id, title, country_id)
	values (7, 'm_example', 3)
SET IDENTITY_INSERT ManufacturerBigIndex OFF
go
--------------------
set statistics time on
set statistics io on
go

dbcc dropcleanbuffers
go
select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBig T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBig M on M.manufacturer_id = T.manufacturer_id
	where T.title = 'example' and
		M.title = 'm_example'

-- (����� ����������: 132044)
-- ������� "ManufacturerBig". ����� ���������� 5, ���������� ������ 1380, ���������� ������ 12, ����������� ������ 1384, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "TyresBig". ����� ���������� 5, ���������� ������ 715, ���������� ������ 9, ����������� ������ 719, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "Worktable". ����� ���������� 0, ���������� ������ 0, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "ResultTyresBig". ����� ���������� 5, ���������� ������ 2277, ���������� ������ 11, ����������� ������ 2277, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.

--  ����� ������ SQL Server:
--    ����� �� = 372 ��, ����������� ����� = 1976 ��.

-------------------------

create nonclustered index [IX_tyres_title] on dbo.TyresBigIndex (
	title asc
)
go
create nonclustered index [IX_manufacturer_title] on dbo.ManufacturerBigIndex (
	title asc
)
go

dbcc dropcleanbuffers
go
select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBigIndex T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBigIndex M on M.manufacturer_id = T.manufacturer_id
	where T.title = 'example' and
		M.title = 'm_example'

-- (����� ����������: 132044)
-- ������� "ManufacturerBigIndex". ����� ���������� 1, ���������� ������ 5, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "Worktable". ����� ���������� 0, ���������� ������ 0, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "TyresBigIndex". ����� ���������� 1, ���������� ������ 5, ���������� ������ 3, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "Worktable". ����� ���������� 0, ���������� ������ 0, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "ResultTyresBig". ����� ���������� 5, ���������� ������ 2277, ���������� ������ 6, ����������� ������ 2283, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.

--  ����� ������ SQL Server:
--    ����� �� = 312 ��, ����������� ����� = 1767 ��.
-------------------------

dbcc dropcleanbuffers
go

select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBig T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBig M on M.manufacturer_id = T.manufacturer_id
	where T.title = 'example' and
		M.title = 'm_example'
		
dbcc dropcleanbuffers
go
		
select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBigIndex T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBigIndex M on M.manufacturer_id = T.manufacturer_id
	where T.title = 'example' and
		M.title = 'm_example'

go
