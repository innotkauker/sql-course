use [Formula 1]
select * into ResultTyresBig from ResultTyres
select * into TyresBig from Tyres
select * into TyresBigIndex from Tyres
select * into ManufacturerBig from Manufacturer
select * into ManufacturerBigIndex from Manufacturer

go

---------------------

INSERT INTO [ResultTyresBig] 
	select stage_id, pilot_id, start_time, tyres_id
	from ResultTyres

go 3000

SET IDENTITY_INSERT TyresBig ON
go
INSERT INTO [TyresBig] (tyres_id, manufacturer_id, type_id, title)
	select tyres_id, manufacturer_id, type_id, title
	from Tyres
go 20000
SET IDENTITY_INSERT TyresBig OFF
go

SET IDENTITY_INSERT TyresBigIndex ON
go
INSERT INTO [TyresBigIndex] (tyres_id, manufacturer_id, type_id, title)
	select tyres_id, manufacturer_id, type_id, title
	from Tyres;
	
go 20000
SET IDENTITY_INSERT TyresBigIndex OFF
go
---------------------
SET IDENTITY_INSERT ManufacturerBig ON
go
INSERT INTO [ManufacturerBig] (manufacturer_id, title, country_id)
	select manufacturer_id, title, country_id
	from Manufacturer
go 20000
SET IDENTITY_INSERT ManufacturerBig OFF
go

SET IDENTITY_INSERT ManufacturerBigIndex ON
go
INSERT INTO [ManufacturerBigIndex] (manufacturer_id, title, country_id)
	select manufacturer_id, title, country_id
	from Manufacturer;
go 20000
SET IDENTITY_INSERT ManufacturerBigIndex OFF
go
--------------------
set statistics time on
set statistics io on
go

dbcc dropcleanbuffers
go
select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBig T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBig M on M.manufacturer_id = T.manufacturer_id
	where T.title like '%[xX]%' and
		M.title = 'NQpmqIVTwfrPD'

-- ������� "ResultTyresBig". ����� ���������� 5, ���������� ������ 2277, ���������� ������ 8, ����������� ������ 1016, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "Worktable". ����� ���������� 4, ���������� ������ 8842674, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "ManufacturerBig". ����� ���������� 4, ���������� ������ 5520, ���������� ������ 0, ����������� ������ 1056, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "TyresBig". ����� ���������� 5, ���������� ������ 715, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "Worktable". ����� ���������� 0, ���������� ������ 0, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.

--  ����� ������ SQL Server:
--    ����� �� = 459690 ��, ����������� ����� = 186464 ��.

-------------------------

create nonclustered index [IX_tyres_title] on dbo.TyresBigIndex (
	title asc
)
go
create nonclustered index [IX_manufacturer_title] on dbo.ManufacturerBigIndex (
	title asc
)
go

dbcc dropcleanbuffers
go
select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBigIndex T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBigIndex M on M.manufacturer_id = T.manufacturer_id
	where T.title like '%[xX]%' and
		M.title = 'NQpmqIVTwfrPD'

-- ������� "ResultTyresBig". ����� ���������� 5, ���������� ������ 2277, ���������� ������ 9, ����������� ������ 2277, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "Worktable". ����� ���������� 4, ���������� ������ 8842674, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "ManufacturerBigIndex". ����� ���������� 4, ���������� ������ 5520, ���������� ������ 8, ����������� ������ 1514, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "TyresBigIndex". ����� ���������� 5, ���������� ������ 715, ���������� ������ 13, ����������� ������ 713, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.
-- ������� "Worktable". ����� ���������� 0, ���������� ������ 0, ���������� ������ 0, ����������� ������ 0, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.

--  ����� ������ SQL Server:
--    ����� �� = 463041 ��, ����������� ����� = 138428 ��.
-------------------------

dbcc dropcleanbuffers
go

select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBig T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBig M on M.manufacturer_id = T.manufacturer_id
	where T.title like '%[xX]%' and
		M.title = 'NQpmqIVTwfrPD'
		
select stage_id, pilot_id, T.title, M.title
	from ResultTyresBig RT join TyresBigIndex T on RT.tyres_id = T.tyres_id 
						   join ManufacturerBigIndex M on M.manufacturer_id = T.manufacturer_id
	where T.title like '%[xX]%' and
		M.title = 'NQpmqIVTwfrPD'

go
